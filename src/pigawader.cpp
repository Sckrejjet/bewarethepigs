#include    "pigawader.h"

#include    "asound.h"

#include    <QHideEvent>
#include    <QKeyEvent>

#include    <QApplication>
#include    <QScreen>


PigAwader::PigAwader(QWidget* parent)
    : QGraphicsView(parent)
    , counter_(0)
{
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setWindowState(Qt::WindowFullScreen);
    setMouseTracking(true);
    setRenderHint(QPainter::Antialiasing);
    setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);

    int w = QApplication::screens().at(0)->geometry().width();
    int h = QApplication::screens().at(0)->geometry().height();

    scene_ = new TheGraphicsScene();
    scene_->setSceneRect(0, 0, w- 200, h- 200);
    scene_->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene_->setBackgroundBrush(QBrush(QColor("#70fff0")));
    scene_->addRect(QRectF(0, 0, scene_->width(), scene_->height()));

    connect(scene_, SIGNAL(sendCursorPoint(int,int)),
            this, SLOT(setCursorPointToPigs(int,int)));

    hero_ = new Hero();
    hero_->setPos(scene_->width()/2, scene_->height()/2);
    scene_->addItem(hero_);

    coin_ = new Coin();
    scene_->addItem(coin_);
    coin_->setPos(coin_->mapFromScene(20, 20));
    coin_->setHero(hero_);

//    for (int i = 0; i < 10; ++i)
//    {
    listPigs_.append(new Pig());
    scene_->addItem(listPigs_.last());
    listPigs_.last()->setPos(40, 40);
//    }

    this->setScene(scene_);

    music_ = new ASound("../bewarethepigs/resources/sounds/music/winter.wav");
    music_->setLoop(true);
    music_->setVolume(50);

    timeSlow_ = new ASound(":/time/slow");
    timeSlow_->setVolume(50);

    timer_ = new QTimer(this);

    connect(timer_, SIGNAL(timeout()),
            scene_, SLOT(advance()));

    connect(timer_, SIGNAL(timeout()),
            this, SLOT(onTimer()));
}

void PigAwader::run()
{
    this->show();
    timer_->start(25);
    music_->play();
}

void PigAwader::hideEvent(QHideEvent *)
{
    timer_->stop();
    music_->pause();
}

void PigAwader::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        this->hide();
    }
    else if (e->key() == Qt::Key_Space)
    {
        hero_->toogleSpeedCoeff();
        for (Pig* pig : listPigs_)
            pig->toogleSpeedCoef();
        if (music_->getPitch() == 1.0)
        {
            music_->setPitch(0.5);
            timeSlow_->play();
        }
        else
            music_->setPitch(1.0);
    }
}

void PigAwader::setCursorPointToPigs(int x, int y)
{
    mouseX_ = x;
    mouseY_ = y;
}

void PigAwader::onTimer()
{
    if (counter_ == 255)
    {
        counter_ = 0;
        if (listPigs_.count() < 15)
        {
            listPigs_.append(new Pig());
            int pigX = hero_->x() > scene_->width()/2 ? 40 : scene_->width() - 40;
            listPigs_.last()->setPos(pigX, 40);
            scene_->addItem(listPigs_.last());
        }
    }
    else
    {
        ++counter_;
    }

    for (Pig* pig : listPigs_)
    {
        pig->setTargetPoint(hero_->x(), hero_->y());
    }

    QLineF lineToPig;
    double minDx = 1000;
    int pigIndex = -1;
    for (int i = 0, n = listPigs_.count(); i < n; ++i)
    {
        if (listPigs_[i]->collidesWithItem(hero_))
        {
            music_->stop();
            timer_->stop();
        }
        lineToPig = QLineF(hero_->pos(), listPigs_[i]->pos());
        if (lineToPig.length() < minDx)
        {
            pigIndex = i;
            minDx = lineToPig.length();
        }
    }

    if (minDx < 150)
    {
        hero_->setTargetPoint(listPigs_[pigIndex]->x(),
                              listPigs_[pigIndex]->y());
    }
    else
    {
        hero_->setTargetPoint(mouseX_, mouseY_);
    }

    hero_->setMovePoint(mouseX_, mouseY_);
}

 void PigAwader::reset_()
 {
     timer_->stop();
     //hero_->setPos();
     // delete pigs!
 }
