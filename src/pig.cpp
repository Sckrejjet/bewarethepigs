#include    "pig.h"

#include    <QGraphicsScene>
#include    <QPainter>
#include    <QStyleOption>
#include    <math.h>
#include    <qmath.h>

constexpr double PI = 3.141592653589793238462;
constexpr int RAND_VAL = 40;

constexpr int maxPointX = 31;
constexpr int minPointX = -31;
constexpr int maxPointY = 20;
constexpr int minPointY = -32;

Pig::Pig()
    : QGraphicsItem()
    , speedCoeff_(1.0)
    , stepX_((qrand()%RAND_VAL/10.0 + 2.0) * (qrand()%2 ? 1 : -1))
    , stepY_((qrand()%RAND_VAL/10.0 + 2.0) * (qrand()%2 ? 1 : -1))
    , targetX_(50)
    , targetY_(50)
{

}



QRectF Pig::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-32 - adjust, -30 - adjust,
                  64 + adjust, 50 + adjust);
}



QPainterPath Pig::shape() const
{
    QPainterPath path;
//    path.addRect(-30, -20, 60, 40);
    path.addRect(-25, -15, 50, 30);
    return path;
}



void Pig::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                QWidget *)
{
    painter->setPen(QColor("#af8080"));
    painter->setBrush(QColor("#ffa0a0"));
    // Уши
    QPainterPath ears;
    ears.moveTo(-28, -5);
    ears.lineTo(-31, -18);
    ears.lineTo(-30, -25);
    ears.lineTo(-28, -32);
    ears.lineTo(-26, -26);
    ears.lineTo(-15, -22);
    ears.lineTo(-5, -15);
    ears.lineTo(5, -15);
    ears.lineTo(15, -22);
    ears.lineTo(26, -26);
    ears.lineTo(28, -32);
    ears.lineTo(30, -25);
    ears.lineTo(31, -18);
    ears.lineTo(28, -5);

//    ears.moveTo(-28, -5);
//    ears.lineTo(-31, -18);
//    ears.lineTo(-30, -25);
//    ears.lineTo(-28, -32);
//    ears.lineTo(-26, -26);
//    ears.lineTo(-5, -15);
//    ears.lineTo(5, -15);
//    ears.lineTo(26, -26);
//    ears.lineTo(28, -32);
//    ears.lineTo(30, -25);
//    ears.lineTo(31, -18);
//    ears.lineTo(28, -5);

    painter->drawPath(ears);

    // Морда
    painter->drawEllipse(-30, -20, 60, 40);


    // Пятак
    painter->setPen(QColor("#000000"));

    painter->setBrush(QColor("#ff5050"));
    painter->drawEllipse(-10, -6, 20, 12);

    // Ноздри
    painter->setBrush(QColor("#000000"));
    painter->drawEllipse(-5, -2, 3, 4);
    painter->drawEllipse(2, -2, 3, 4);

    // Белки глаз
    painter->setBrush(QColor("#ffffff"));
    painter->drawEllipse(-25, -20, 15, 15);
    painter->drawEllipse(10, -20, 15, 15);

    //
    double catX = targetX_ - x();
    double catY = targetY_ - y() + 15;

    double hypo = sqrt(catX*catX + catY*catY);

    double cosA = catX / hypo;
    double sinA = catY / hypo;


    // Рот
    if (abs(hypo) < 150 )
    {
        painter->drawPie(-15, 7, 30, 10, 160 * 16, 220 * 16);
    }
    else
    {
        painter->drawPie(-15, 10, 30, 3, 180 * 16, 180 * 16);
    }

    // Зрачки
    double rad = abs(hypo) > 150 ? 5 : 7;
    double paddingX = 30;

    int leftEye0 = 0 - paddingX/2 - rad;
    int rightEye0 = paddingX/2;

    double leftEye = leftEye0 + 5*cosA;
    double rightEye = rightEye0 + 5*cosA;

    int paddingY0 = -10 - rad;
    int paddingY = paddingY0 + 5*sinA;

    painter->setBrush(QColor("#000000"));
    painter->drawEllipse(leftEye, paddingY, rad, rad);
    painter->drawEllipse(rightEye, paddingY, rad, rad);

    // Брови
    painter->setPen(QPen(Qt::black, 1));

    int y2 = hypo < 150 ? -20 : -22;
    painter->drawLine(-25, -23, -10, y2);
    painter->drawLine(25, -23, 10, y2);
}

void Pig::setTargetPoint(int x, int y)
{
    targetX_ = x;
    targetY_ = y;
}



void Pig::toogleSpeedCoef()
{
    if (speedCoeff_ > 0.9 && speedCoeff_ < 1.1)
    {
        speedCoeff_ = 0.3;
    }
    else
    {
        speedCoeff_ = 1.0;
    }
}



void Pig::advance(int step)
{
    if (!step)
        return;

    QGraphicsScene* scene = this->scene();

//    if (this->x() > 500)
    if (this->x() + maxPointX > scene->sceneRect().width())
        stepX_ = -(qrand()%RAND_VAL/10.0 + 1);

//    if (this->x() < -500)
    if (this->x() + minPointX < scene->sceneRect().x())
        stepX_ = (qrand()%RAND_VAL/10.0 + 1);

//    if (this->y() > 300)
    if (this->y() + maxPointY > scene->sceneRect().height())
        stepY_ = -(qrand()%RAND_VAL/10.0 + 1);

//    if (this->y() < -300)
    if (this->y() + minPointY < scene->sceneRect().y())
        stepY_ = (qrand()%RAND_VAL/10.0 + 1);


    setPos(mapToParent(stepX_*speedCoeff_, stepY_*speedCoeff_));
}
