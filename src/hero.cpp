#include    "hero.h"

#include    <QGraphicsScene>
#include    <QPainter>
#include    <QStyleOption>
#include    <math.h>
#include    <qmath.h>

//constexpr double PI = 3.141592653589793238462;
constexpr int RAND_VAL = 2;

Hero::Hero()
    : QGraphicsItem()
    , speedCoeff_(1.0)
    , step_(4.0)
    , targetX_(50)
    , targetY_(50)
    , moveX_(0.0)
    , moveY_(0.0)
{

}



QRectF Hero::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-15 - adjust, -16 - adjust,
                  30 + adjust, 30 + adjust);
}



QPainterPath Hero::shape() const
{
    QPainterPath path;
    path.addRect(-10, -10, 20, 20);
//    path.addRect(-15, -15, 30, 30);
    return path;
}



void Hero::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                QWidget *)
{
//    painter->setPen(QColor("#af8080"));
//    painter->setBrush(QColor("#ffa0a0"));
//    // Уши
//    QPainterPath ears;
//    ears.moveTo(-28, -5);
//    ears.lineTo(-31, -18);
//    ears.lineTo(-30, -25);
//    ears.lineTo(-28, -32);
//    ears.lineTo(-26, -26);
//    ears.lineTo(-15, -22);
//    ears.lineTo(-5, -15);
//    ears.lineTo(5, -15);
//    ears.lineTo(15, -22);
//    ears.lineTo(26, -26);
//    ears.lineTo(28, -32);
//    ears.lineTo(30, -25);
//    ears.lineTo(31, -18);
//    ears.lineTo(28, -5);

//    ears.moveTo(-28, -5);
//    ears.lineTo(-31, -18);
//    ears.lineTo(-30, -25);
//    ears.lineTo(-28, -32);
//    ears.lineTo(-26, -26);
//    ears.lineTo(-5, -15);
//    ears.lineTo(5, -15);
//    ears.lineTo(26, -26);
//    ears.lineTo(28, -32);
//    ears.lineTo(30, -25);
//    ears.lineTo(31, -18);
//    ears.lineTo(28, -5);

//    painter->drawPath(ears);

    // Морда
    painter->setBrush(QColor("#000000"));
    painter->drawEllipse(-10, -10, 20, 20);


//    // Пятак
//    painter->setPen(QColor("#000000"));

//    painter->setBrush(QColor("#ff5050"));
//    painter->drawEllipse(-10, -6, 20, 12);

//    // Ноздри
//    painter->setBrush(QColor("#000000"));
//    painter->drawEllipse(-5, -2, 3, 4);
//    painter->drawEllipse(2, -2, 3, 4);

    // Белки глаз
    painter->setBrush(QColor("#ffffff"));
    painter->drawEllipse(-15, -12, 15, 15);
    painter->drawEllipse(0, -12, 15, 15);

    //
    double catX = targetX_ - x();
    double catY = targetY_ - y() + 15;

    double hypo = sqrt(catX*catX + catY*catY);
    bool fear = targetX_ != moveX_ || targetY_ != moveY_;

    double cosA = catX / hypo;
    double sinA = catY / hypo;


    // Рот
    painter->setPen(QPen(Qt::white, 1));
    if (hypo < 150 && fear)
    {
        painter->drawPie(-3, 6, 6, 4, 0 * 16, 180 * 16);
    }
    else
    {
        painter->drawPie(-3, 6, 6, 1, 180 * 16, 182 * 16);
    }

    // Зрачки
    painter->setPen(Qt::black);
    double rad = (hypo < 150) && (fear) ? 7 : 5;
    double paddingX = (32 - rad)/2;

    //int leftEye0 = 0 - paddingX/2 - rad;
    int leftEye0 = 0 - paddingX/2 - rad;
    int rightEye0 = paddingX/2;

    double leftEye = leftEye0 + rad/2*cosA;
    double rightEye = rightEye0 + rad/2*cosA;

    int paddingY0 = -2 - rad;
    int paddingY = paddingY0 + 5*sinA;

    painter->setBrush(QColor("#000000"));
    painter->drawEllipse(leftEye, paddingY, rad, rad);
    painter->drawEllipse(rightEye, paddingY, rad, rad);

    // Брови
//    painter->setPen(QPen(Qt::black, 1));

    int y2 = hypo < 150 && fear ? -17 : -15;
    //int y2 = -17;
    painter->drawLine(-15, -13, -5, y2);
    painter->drawLine(15, -13, 5, y2);
}

void Hero::setTargetPoint(int x, int y)
{
    targetX_ = x;
    targetY_ = y;
}

void Hero::setMovePoint(int x, int y)
{
    moveX_ = x;
    moveY_ = y;
}



void Hero::toogleSpeedCoeff()
{
    if (speedCoeff_ > 0.9 && speedCoeff_ < 1.1)
    {
        speedCoeff_ = 1.5;
    }
    else
    {
        speedCoeff_ = 1.0;
    }
}



void Hero::advance(int step)
{
    if (!step)
        return;

    double catX = moveX_ - this->x();
    double catY = moveY_ - this->y();

    double way = sqrt(catX*catX + catY*catY);

    if (way > 3)
    {
        double stX = catX/(way/step_);
        double stY = catY/(way/step_);
        setPos(mapToParent(stX*speedCoeff_, stY*speedCoeff_));
    }

}
