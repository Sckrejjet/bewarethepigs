#include    "the-graphics-scene.h"

#include    <QGraphicsSceneMouseEvent>

TheGraphicsScene::TheGraphicsScene(QObject *parent)
    : QGraphicsScene(parent)
{

}

void TheGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *e)
{
    emit sendCursorPoint(e->scenePos().x(), e->scenePos().y());
}
