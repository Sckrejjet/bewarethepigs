#include    "coin.h"

#include    "hero.h"
#include    "asound.h"

#include    <QGraphicsScene>
#include    <QPainter>
#include    <QStyleOption>
#include    <math.h>
#include    <qmath.h>


constexpr int RAND_VAL = 2;

const QString SOUND_PATH = ":/coin/coin";


Coin::Coin()
    : QGraphicsItem()
    , hero_(Q_NULLPTR)
{
    for (int i = 0; i < 3; ++i)
    {
        listSounds_.append(new ASound(SOUND_PATH + QString::number(i)));
    }
}



QRectF Coin::boundingRect() const
{
    qreal adjust = 1.5;
    return QRectF(-5 - adjust, -5 - adjust,
                  10 + adjust, 10 + adjust);
}



QPainterPath Coin::shape() const
{
    QPainterPath path;
    //path.addRect(-5, -5, 20, 20);
    path.addEllipse(-5, -5, 10, 10);
//    path.addRect(-15, -15, 30, 30);
    return path;
}



void Coin::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                QWidget *)
{
    painter->setBrush(QColor("#efff00"));
    painter->drawEllipse(-5, -5, 10, 10);
}


void Coin::setHero(Hero *hero)
{
    hero_ = hero;
}


void Coin::respawn()
{
    if (scene())
    {
        int w = scene()->sceneRect().width();
        int h = scene()->sceneRect().height();
        this->setPos(qrand()%w, qrand()%h);
    }
}


void Coin::advance(int step)
{
    if (!step)
        return;

    if (hero_ && collidesWithItem(hero_))
    {
        int w = scene()->sceneRect().width();
        int h = scene()->sceneRect().height();
        this->setPos(qrand()%w, qrand()%h);
        listSounds_[qrand()%3]->play();
        //setPos(4, 4);
    }

    //setPos(mapToParent(this->x(), this->y()));
}
