#include "mainwidget.h"
#include "asound.h"

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
{
    AListener listener = AListener::getInstance();
    Q_UNUSED(listener);

    buttonRunPigAwader_ = new QPushButton("Pig Awader", this);
    buttonRunPigAwader_->setObjectName("Pig Awader");

    connect(buttonRunPigAwader_, SIGNAL(clicked(bool)),
            this, SLOT(handleButtons()));

    pigAwader_ = new PigAwader();
}

MainWidget::~MainWidget()
{
    AListener listener = AListener::getInstance();
    listener.closeDevices();
}

void MainWidget::handleButtons()
{
    QPushButton* button = (QPushButton*) sender();
    if (button->objectName() == "Pig Awader")
    {
        pigAwader_->run();
    }
}
