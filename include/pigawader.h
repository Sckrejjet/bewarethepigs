#ifndef PIGAWADER_H
#define PIGAWADER_H

#include    <QGraphicsView>
#include    <QTimer>
#include    "hero.h"
#include    "pig.h"
#include    "coin.h"
#include    "the-graphics-scene.h"

class ASound;

class PigAwader : public QGraphicsView
{
    Q_OBJECT
public:
    PigAwader(QWidget* parent = Q_NULLPTR);

    void run();

public slots:
    void hideEvent(QHideEvent* e);

    void keyPressEvent(QKeyEvent* e);

    void setCursorPointToPigs(int x, int y);

    void onTimer();

private:

    uint8_t counter_;

    QTimer* timer_;

    TheGraphicsScene *scene_;

    Hero* hero_;

    Coin* coin_;

    QList<Pig*> listPigs_;

    int mouseX_;
    int mouseY_;

    ASound* music_;

    ASound* timeSlow_;

    void reset_();
};

#endif // PIGAWADER_H
