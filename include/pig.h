#ifndef PIG_H
#define PIG_H

#include    <QGraphicsItem>

class Pig : public QGraphicsItem
{
public:
    Pig();


    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;

    void setTargetPoint(int x, int y);

    void toogleSpeedCoef();

protected:
    void advance(int step) Q_DECL_OVERRIDE;


private:
    double speedCoeff_;

    double stepX_;
    double stepY_;

    int targetX_;
    int targetY_;
};

#endif // PIG_H
