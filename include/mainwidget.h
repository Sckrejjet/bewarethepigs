#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QPushButton>

#include "pigawader.h"

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = 0);
    ~MainWidget();

public slots:
    void handleButtons();

private:
    QPushButton* buttonRunPigAwader_;

    PigAwader* pigAwader_;

};

#endif // MAINWIDGET_H
