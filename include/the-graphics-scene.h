#ifndef THEGRAPHICSSCENE_H
#define THEGRAPHICSSCENE_H

#include    <QGraphicsScene>

class TheGraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    TheGraphicsScene(QObject* parent = Q_NULLPTR);

signals:
    void sendCursorPoint(int x, int y);

public slots:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* e);
};
#endif // THEGRAPHICSSCENE_H
