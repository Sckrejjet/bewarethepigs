#ifndef HERO_H
#define HERO_H

#include    <QGraphicsItem>

class Hero : public QGraphicsItem
{
public:
    Hero();


    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;

    void setTargetPoint(int x, int y);

    void setMovePoint(int x, int y);

    void toogleSpeedCoeff();

protected:
    void advance(int step) Q_DECL_OVERRIDE;


private:
    double speedCoeff_;
    double step_;

    int targetX_;
    int targetY_;

    int moveX_;
    int moveY_;
};

#endif // HERO_H
