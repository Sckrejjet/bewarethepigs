#ifndef COIN_H
#define COIN_H

#include    <QGraphicsItem>


class Hero;
class ASound;


class Coin : public QGraphicsItem
{
public:
    Coin();


    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;

    void setHero(Hero* hero);

    void respawn();


protected:
    void advance(int step) Q_DECL_OVERRIDE;


private:
    double angle_;

    Hero* hero_;

    QList<ASound*> listSounds_;

};

#endif // COIN_H
