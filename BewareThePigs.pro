#-------------------------------------------------
#
# Project created by QtCreator 2017-07-17T21:06:43
#
#-------------------------------------------------

QT       += core gui

QMAKE_RESOURCE_FLAGS += -compress 0

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BewareThePigs

TEMPLATE = app

CONFIG(debug, debug|release){
    TARGET = BewareThePigs_d
    LIBS += -L'../../../work/vr/lastochka/soft/lib' -lASoundd
} else {
    TARGET = BewareThePigs
    LIBS += -L'../../../work/vr/lastochka/soft/lib' -lASound
}

DESTDIR = ../bin

win32{
    LIBS += -L'C:/Program Files (x86)/OpenAL 1.1 SDK/libs/Win64/' -lOpenAL32
    INCLUDEPATH += 'C:/Program Files (x86)/OpenAL 1.1 SDK/include'
}

unix{
    LIBS += -lopenal
    INCLUDEPATH += /usr/include/AL
}

INCLUDEPATH += ../../../work/vr/lastochka/soft/ASound/include

INCLUDEPATH += include/
VPATH += src/

HEADERS  += $$files(include/*.h)

SOURCES += $$files(src/*.cpp)

RESOURCES += \
    resources/sounds.qrc
